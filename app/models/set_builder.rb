class SetBuilder
  def initialize
    @set = []
  end

  attr_reader :set

  def show_set
    set
  end

  def add(number)
    if (validate_new_element(number))
      @set << number
      @set
    else
      puts 'Elements in set must unique'
    end
  end

  def remove(number)
    @set.reject!{|a| a == number }
    set
  end

  def check_exist(number)
    @set.include?(number)
  end

  private

  def validate_new_element(number)
    return false if check_exist(number)
    true
  end
end

class Test

  def initialize

  end

  def reverse_array(array)
    swap(array, 0, array.size - 1)
    array
  end

  private

  def swap(array, head, tail)
    if head < tail
      temp = array[head]
      array[head] = array[tail]
      array[tail] = temp

      swap(array, head + 1, tail - 1)
    end
  end
end
